const linkLi = document.querySelectorAll('.nav-ul-link-li');
const section4ContentGroups = document.querySelectorAll('.section-4-content-group');
const section5ContentGroups = document.querySelectorAll('.section-5-content-group');


linkLi.forEach((li) => {

    li.addEventListener('click', function() {
        

        linkLi.forEach((li) => {
            li.classList.remove('link-active');
        });

        this.classList.add('link-active');
        const currentTab = this.getAttribute('data-tab');

        document.querySelectorAll('.section-2-nav-ul-targets p').forEach((tab) => {
            tab.classList.remove('tab-active');
            tab.classList.add('tab-inactive');
        });

        const targetTab = document.querySelector('.' + currentTab);
        targetTab.classList.remove('tab-inactive');
        targetTab.classList.add('tab-active');
    });
});

section4ContentGroups.forEach((group) => {
    group.addEventListener('click', function() {
        section4ContentGroups.forEach((group) => {
            group.classList.remove('group-active');
        });

        this.classList.add('group-active');
    });
});

section5ContentGroups.forEach((group) => {
    group.addEventListener('click', function() {
        section5ContentGroups.forEach((group) => {
            group.classList.remove('section-5-group-active');
        });

        this.classList.add('section-5-group-active');
    });
});
